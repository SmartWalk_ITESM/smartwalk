from django import forms
from django.forms import ModelForm
from .models import User

class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = ['username']

class StepsForm(ModelForm):
    class Meta:
        model = User
        exclude = ['username']