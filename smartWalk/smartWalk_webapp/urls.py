from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.index),
    url(r'^set_steps_counter$', views.set_steps_counter, name='user_config'),
    url(r'^id/(?P<device_id>[A-Z0-9]{5})/$', views.get_config, name='get_config'),
]