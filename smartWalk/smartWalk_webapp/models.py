from django.db import models

class User(models.Model):
    username = models.CharField('username', max_length=20, unique=True)
    step_counter = models.IntegerField()
    
    def __str__(self):
        return self.username

class Device(models.Model):
    owner = models.ForeignKey(User)
    device_id = models.CharField('Device ID', max_length=5, unique=True)
    
    def __str__(self):
        return self.device_id
