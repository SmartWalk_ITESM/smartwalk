from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound
from django.http import Http404

from .models import User, Device
from .forms import LoginForm, StepsForm

from django.contrib.auth import authenticate

DEFAULT_STEPS = 100

def index(request):
    loginForm = LoginForm()
    if request.method == "POST":
        loginForm = LoginForm(request.POST)
        user = User.objects.filter(username=loginForm['username'].value()).first()
        if user:
            request.session['username'] = user.username
        
    if 'username' in request.session:
        return redirect('user_config')

    return render(request, 'login.html', {'form': LoginForm})

def set_steps_counter(request):
    if not 'username' in request.session:
        return redirect('index')
    user = User.objects.filter(username=request.session['username']).first()
    if not user:
        return redirect('index')
    
    stepsForm = StepsForm(request.POST or None, instance = user)

    if stepsForm.is_valid():
        user.step_counter= stepsForm['step_counter'].value()
        user.save()

    return render(request, 'step_counter.html', {'form': stepsForm})

def get_config(request, device_id=0):
    if(device_id == 0):
        return HttpResponseNotFound("no device")
    device = Device.objects.filter(device_id=device_id).first()
    if not device:
        return HttpResponseNotFound("no device")
    steps_to_count = device.owner.step_counter
    if not steps_to_count:
        steps_to_count = DEFAULT_STEPS
    temperature = 31 # TODO: check local weather
    return HttpResponse("{{%d,%d}}"%(steps_to_count, temperature), content_type="application/txt")